process.env.NODE_ENV = 'test'
const app = require('../../app.js')
var Puppy = require('../../models/puppy.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
const find = require('lodash.find')
const remove = require('lodash.remove')
const ID = 9999999
const NAME = 'Eddie'
const BREED = 'lab'
const AGE = 2
const NEGATIVE_AGE = -2
const NON_INT_AGE = 2.5

LOG.debug('Starting test/model/puppy.spec.js.')

mocha.describe('API Tests - Puppy Model', function () {
  mocha.before(function (done) {
    const data = app.locals.puppies.query
    LOG.debug('Before the test, there are %s puppies', data.length)
    const item = new Puppy({
      _id: ID,
      name: NAME,
      breed: BREED
    })
    data.push(item)
    LOG.debug('Before the test, we added another for a total of %s puppies', data.length)
    done()
  })
  mocha.after(function (done) {
    const data = app.locals.puppies.query
    LOG.debug('After the test, there are %s puppies', data.length)
    for (let i = 0; i < data.length; i++) {
      LOG.debug(data[i]._id)
    }
    const item = find(data, { '_id': ID })
    if (!item) {
      LOG.error('The item added during the test was not found. _id = ', ID)
    }
    const deletedItem = remove(data, { '_id': ID })
    LOG.info(`Permanently deleted item ${JSON.stringify(deletedItem)}`)
    LOG.debug('After test cleanup, there are %s puppies', data.length)
    done()
  })
  mocha.it('new item should not be null', function (done) {
    const data = app.locals.puppies.query
    const item = find(data, { '_id': ID })
    expect(item).to.not.equal(null)
    done()
  })
  mocha.it('it should have an _id', function (done) {
    const data = app.locals.puppies.query
    const item = find(data, { '_id': ID })
    expect(item._id).to.not.equal(null)
    LOG.debug('item._id = ' + item._id)
    done()
  })
  mocha.it('it should have the given breed', function (done) {
    const data = app.locals.puppies.query
    const item = find(data, { '_id': ID })
    expect(item.breed).to.equal(BREED)
    LOG.debug('Puppy.breed = ' + item.breed)
    done()
  })
})

mocha.describe('API Tests - Puppy Model Invalid', function () {
  mocha.it('it should be invalid if _id is empty', function (done) {
    var testItem = new Puppy({
      breed: BREED,
      age: AGE
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
  mocha.it('it should be invalid if name is empty', function (done) {
    var testItem = new Puppy({
      breed: BREED
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
  mocha.it('it should be invalid if age is negative', function (done) {
    var testItem = new Puppy({
      name: NAME,
      breed: BREED,
      age: NEGATIVE_AGE
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
  mocha.it('it should be invalid if age is not an integer', function (done) {
    var testItem = new Puppy({
      name: NAME,
      breed: BREED,
      age: NON_INT_AGE
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
})
